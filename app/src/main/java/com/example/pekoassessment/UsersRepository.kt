package com.example.pekoassessment

import com.example.pekoassessment.model.CommonResponse
import com.example.pekoassessment.model.UserDetailsResponse
import com.example.pekoassessment.model.UserReposItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val apiInterface: ApiInterface
) {

    suspend fun getSearchedUsers(
        query: String,
        page : Int,
        per_page : Int
    ) : Flow<Result<CommonResponse>>{
        return flow {
            emit(
                safeApiCall{
                    apiInterface.
                    getSearchedUsers(query,page,per_page)
                }

            )
        }
    }


    suspend fun getUserDetails(
        userName : String
    ) : Flow<Result<UserDetailsResponse>>{
        return flow {
            emit(
                safeApiCall{
                    apiInterface.
                    getUserDetails(userName)
                }

            )
        }
    }

    suspend fun getUserRepos(
        userName : String
    ) : Flow<Result<ArrayList<UserReposItem>>>{
        return flow {
            emit(
                safeApiCall{
                    apiInterface.
                    getUserRepos(userName)
                }

            )
        }
    }



}