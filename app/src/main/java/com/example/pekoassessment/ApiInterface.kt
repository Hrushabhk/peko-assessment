package com.example.pekoassessment

import com.example.pekoassessment.model.CommonResponse
import com.example.pekoassessment.model.UserDetailsResponse
import com.example.pekoassessment.model.UserReposItem
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
//  https://api.github.com/search/users?q=a&&page=1&&per_page=10
    @GET("/search/users")
    suspend fun getSearchedUsers(
        @Query("q") q: String,
        @Query("page") page : Int,
        @Query("per_page") per_page : Int
    ) : CommonResponse


//    https://api.github.com/users/unkaktus
    @GET("/users/{name}")
    suspend fun getUserDetails(
        @Path("name") name : String
    ):UserDetailsResponse

//    https://api.github.com/users/unkaktus/repos
    @GET("/users/{name}/repos")
    suspend fun getUserRepos(
        @Path("name") name : String
    ):ArrayList<UserReposItem>
}