package com.example.pekoassessment.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pekoassessment.R
import com.example.pekoassessment.Result
import com.example.pekoassessment.databinding.ActivityAllUsersBinding
import com.example.pekoassessment.model.CommonResponse
import com.example.pekoassessment.model.UserItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AllUsersActivity : AppCompatActivity() {
    lateinit var binding : ActivityAllUsersBinding
    lateinit var adapterGitHubUsers: AdapterGitHubUsers
    var userList = ArrayList<UserItem> ()

    private val viewModel: UsersViewModel by viewModels()
    lateinit var commonResponse : CommonResponse
    var searchedQuery = ""
    var pg_no = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_all_users)

        binding.searchEt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                userList.clear()
                searchedQuery = p0.toString()
                pg_no = 1

                if(p0.toString().isNotEmpty()){
                    getSearchedUsers(p0.toString())
                }else{
                    userList.clear()
                    adapterGitHubUsers.notifyDataSetChanged()
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        observers()
        setAdapter()

        binding.recAllUsers.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (linearLayoutManager?.findLastCompletelyVisibleItemPosition() == userList.size - 1 && userList.size != commonResponse.total_count) {
                    pg_no += 1
                    getSearchedUsers(searchedQuery)
                }

            }
        })
    }

    private fun observers() {
        viewModel.searchedUserLiveData.observe(this, Observer { response ->
            when (response) {
                is Result.Success -> {
                    if (response.data !=null) {
                        commonResponse = response.data
                        userList.addAll(response.data.items)
                        adapterGitHubUsers.notifyDataSetChanged()
                    } else {
                        Toast.makeText(this,response.data.message?:resources.getString(R.string.something),Toast.LENGTH_SHORT).show()
                    }
                }

                is Result.Error -> {
                    Toast.makeText(this,resources.getString(R.string.something),Toast.LENGTH_SHORT).show()
                }

                is Result.Loading -> {

                }
            }
        })

    }

    private fun setAdapter() {
        binding.recAllUsers.layoutManager = LinearLayoutManager(this)
        adapterGitHubUsers = AdapterGitHubUsers(this,userList)
        binding.recAllUsers.adapter = adapterGitHubUsers
    }

    fun getSearchedUsers(query : String){
        viewModel.getSearchedUsers(query,pg_no,15)
    }
}