package com.example.pekoassessment.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pekoassessment.R
import com.example.pekoassessment.Result
import com.example.pekoassessment.databinding.ActivityUserDetailsBinding
import com.example.pekoassessment.model.UserDetailsResponse
import com.example.pekoassessment.model.UserReposItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserDetailsActivity : AppCompatActivity() {
    lateinit var binding: ActivityUserDetailsBinding
    var userName = ""
    private val viewModel: UsersViewModel by viewModels()
    lateinit var adapterUserRepos: AdapterUserRepos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_details)
        userName = intent.getStringExtra("Name")?:""

        getUserDetails()
        getUserRepos()
        observers()

        binding.imgBack.setOnClickListener {
            finish()
        }
    }

    private fun getUserRepos() {
        viewModel.getUserRepos(userName)
    }

    private fun observers() {
        viewModel.userDetailsLiveData.observe(this, Observer { response ->
            when (response) {
                is Result.Success -> {
                    if (response.data !=null) {
                        setData(response.data)
                    } else {
                        Toast.makeText(this,resources.getString(R.string.something),
                            Toast.LENGTH_SHORT).show()
                    }
                }

                is Result.Error -> {
                    Toast.makeText(this,resources.getString(R.string.something), Toast.LENGTH_SHORT).show()
                }

                is Result.Loading -> {

                }
            }
        })

        viewModel.userReposLiveData.observe(this, Observer { response ->
            when (response) {
                is Result.Success -> {
                    if (response.data !=null) {
                        setAdapter(response.data)
                    } else {
                        Toast.makeText(this,resources.getString(R.string.something),
                            Toast.LENGTH_SHORT).show()
                    }
                }

                is Result.Error -> {
                    Toast.makeText(this,resources.getString(R.string.something), Toast.LENGTH_SHORT).show()
                }

                is Result.Loading -> {

                }
            }
        })
    }

    private fun setAdapter(dataList: ArrayList<UserReposItem>) {
        binding.recRepos.layoutManager = LinearLayoutManager(this)
        adapterUserRepos = AdapterUserRepos(this,dataList)
        binding.recRepos.adapter = adapterUserRepos
    }

    private fun setData(data: UserDetailsResponse) {
        binding.bio.text = "Bio:- ${data.bio}"
        binding.userName.text = "UserName :- ${data.name}"
        binding.loginName.text = "Login Name:- ${data.login}"
        binding.noOfFollowers.text = "No of Followers:- ${data.followers.toString()}"
        binding.noOfRepositories.text = "No Of repos:- ${data.public_repos.toString()}"
        Glide.with(this)
            .load(data.avatar_url)
            .into(binding.avatarImg)
    }

    private fun getUserDetails() {
        viewModel.getUserDetails(userName)
    }
}