package com.example.pekoassessment.presentation
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pekoassessment.Result
import com.example.pekoassessment.UsersRepository
import com.example.pekoassessment.model.CommonResponse
import com.example.pekoassessment.model.UserDetailsResponse
import com.example.pekoassessment.model.UserReposItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val usersRepository: UsersRepository
) : ViewModel(){
    val searchedUserLiveData = MutableLiveData<Result<CommonResponse>>()
    val userDetailsLiveData = MutableLiveData<Result<UserDetailsResponse>>()
    val userReposLiveData = MutableLiveData<Result<ArrayList<UserReposItem>>>()


    fun getSearchedUsers(
        query: String,
        page : Int,
        per_page : Int
    ){
        viewModelScope.launch{
            usersRepository.getSearchedUsers(query, page,per_page).collectLatest {
                searchedUserLiveData.value = it
            }
        }
    }


    fun getUserDetails(
        userName : String
    ){
        viewModelScope.launch{
            usersRepository.getUserDetails(userName).collectLatest {
                userDetailsLiveData.value = it
            }
        }
    }

    fun getUserRepos(
        userName : String
    ){
        viewModelScope.launch{
            usersRepository.getUserRepos(userName).collectLatest {
                userReposLiveData.value = it
            }
        }
    }


}