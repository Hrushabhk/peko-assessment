package com.example.pekoassessment.presentation

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.material3.contentColorFor
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pekoassessment.R
import com.example.pekoassessment.databinding.AdapterGithubUserBinding
import com.example.pekoassessment.databinding.AdapterUserReposBinding
import com.example.pekoassessment.model.UserItem
import com.example.pekoassessment.model.UserReposItem


class AdapterUserRepos(val context: Context,val list :ArrayList<UserReposItem>):RecyclerView.Adapter<AdapterUserRepos.ViewHolder>()  {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_user_repos,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.descriptionRepo.text = "Description:- ${list.get(position).description?:"None"}"
        holder.binding.name.text = "Name:- ${list.get(position).full_name?:"None"}"
        holder.binding.language.text = "Language :-${list.get(position).language?:"None"}"
    }

    class ViewHolder(val binding :AdapterUserReposBinding):RecyclerView.ViewHolder(binding.root)

}