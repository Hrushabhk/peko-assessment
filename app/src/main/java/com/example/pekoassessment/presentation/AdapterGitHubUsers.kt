package com.example.pekoassessment.presentation

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.material3.contentColorFor
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pekoassessment.R
import com.example.pekoassessment.databinding.AdapterGithubUserBinding
import com.example.pekoassessment.model.UserItem


class AdapterGitHubUsers(val context: Context, val list : ArrayList<UserItem>):RecyclerView.Adapter<AdapterGitHubUsers.ViewHolder>()  {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_github_user,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.userName.text = list.get(position).login
        Glide.with(context)
            .load(list.get(position).avatar_url)
            .into(holder.binding.avatarImg)

        holder.binding.root.setOnClickListener{
            val intent = Intent(context,UserDetailsActivity::class.java)
            intent.putExtra("Name",list.get(position).login)
            context.startActivity(intent)
        }
    }

    class ViewHolder(val binding :AdapterGithubUserBinding):RecyclerView.ViewHolder(binding.root)

}