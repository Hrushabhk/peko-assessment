package com.example.pekoassessment

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

suspend fun <T> safeApiCall(
    apiCall: suspend () -> T

): Result<T> {
    return withContext(Dispatchers.IO) {
        try {
            Result.Success(apiCall.invoke())

        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    Result.Error(throwable)
                }

                else -> {
                    Result.Error(throwable)
                }
            }
        }
    }
}