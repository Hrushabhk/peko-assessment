package com.example.pekoassessment.model

import com.google.gson.annotations.SerializedName

data class UserDetailsResponse (
    @SerializedName("login")
    var login : String?,
    @SerializedName("avatar_url")
    var avatar_url : String?,
    @SerializedName("name")
    var name : String?,
    @SerializedName("bio")
    var bio : String?,
    @SerializedName("followers")
    var followers : Int?,
    @SerializedName("public_repos")
    var public_repos : Int?,
)