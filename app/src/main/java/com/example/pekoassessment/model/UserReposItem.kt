package com.example.pekoassessment.model

import com.google.gson.annotations.SerializedName

data class UserReposItem (
    @SerializedName("full_name")
    var full_name : String?,
    @SerializedName("description")
    var description : String?,
    @SerializedName("language")
    var language :String?
)
