package com.example.pekoassessment.model

import com.google.gson.annotations.SerializedName

data class UserItem (
    @SerializedName("login")
    val login : String,
    @SerializedName("avatar_url")
    val avatar_url : String
)
