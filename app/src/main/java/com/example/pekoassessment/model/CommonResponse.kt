package com.example.pekoassessment.model

import com.google.gson.annotations.SerializedName

data class CommonResponse (
    @SerializedName("success")
    val success : Boolean,
    @SerializedName("message")
    val message : String?,
    @SerializedName("errorType")
    val errorType : Int?,
    @SerializedName("total_count")
    val total_count : Int,
    @SerializedName("items")
    val items : ArrayList<UserItem>
)
